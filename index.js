let visor = document.querySelector('.visor');
let visorImage = document.querySelector('.visor-image');
let image = document.querySelector('img');

let hammer = new Hammer(visorImage);
hammer.get('pan').set({ direction: Hammer.DIRECTION_ALL });

let pan = { x : 0, y : 0 };
let bounds = {};

image.addEventListener('load', () => {
    bounds = {
        translateX : image.clientLeft,
        translateY : image.clientTop,
        scaleX: 1,
        scaleY: 1,
        width: image.clientWidth,
        height: image.clientHeight,
        minScaleX: 0.5,
        minScaleY: 0.5,
        maxScaleX: 2,
        maxScaleY: 2
    };

    initEvents();
    move(bounds, 0, 0);
    boundsToTransform(bounds);
});

function boundsToTransform(bounds) {
    visorImage.style.transform = 'translate3d('
        + bounds.translateX + 'px, '
        + bounds.translateY + 'px, 0) scale3d(' + bounds.scaleX + ', ' + bounds.scaleY + ', 1)';
}

function move(bounds, deltaX, deltaY) {
    bounds.translateX += deltaX;
    bounds.translateY += deltaY;

    if (bounds.width > visor.clientWidth) {
        if (bounds.translateX > 0) {
            bounds.translateX = 0;
        }

        if (bounds.translateX + bounds.width < visor.clientWidth) {
            bounds.translateX = visor.clientWidth - bounds.width;
        }
    } else {
        bounds.translateX = (visor.clientWidth - bounds.width) / 2;
    }

    if (bounds.height > visor.clientHeight) {
        if (bounds.translateY > 0) {
            bounds.translateY = 0;
        }

        if (bounds.translateY + bounds.height < visor.clientHeight) {
            bounds.translateY = visor.clientHeight - bounds.height;
        }
    } else {
        bounds.translateY = (visor.clientHeight - bounds.height) / 2;
    }
}

function zoom(bounds, deltaX, deltaY, originX, originY) {
    let moveX = 0;
    let moveY = 0;
    let lastScaleX = bounds.scaleX;
    let lastScaleY = bounds.scaleY;
    let lastWidth = bounds.width;
    let lastHeight = bounds.height;

    bounds.scaleX = bounds.scaleX + deltaX;
    bounds.scaleY = bounds.scaleY + deltaY;
    bounds.width = image.clientWidth * bounds.scaleX;
    bounds.height = image.clientHeight * bounds.scaleY;

    let center = { x : visorImage.clientWidth / 2, y : visorImage.clientHeight / 2 };

    if (originX) {
        let pointOfImage = originX - bounds.translateX;
        let futurePointOfImage = (pointOfImage * bounds.width) / lastWidth;
        moveX = pointOfImage - futurePointOfImage;
    }

    if (originY) {
        let pointOfImage = originY - bounds.translateY;
        let futurePointOfImage = (pointOfImage * bounds.height) / lastHeight;
        moveY = pointOfImage - futurePointOfImage;
    }

    move(bounds, moveX, moveY);
}

function initEvents() {
    window.addEventListener('resize', () => {
        bounds.width = image.clientWidth * bounds.scaleX;
        bounds.height = image.clientHeight * bounds.scaleY;
        move(bounds, 0, 0);
        boundsToTransform(bounds);
    });

    window.addEventListener('wheel', event => {
        let delta = 0.05;
        if (event.deltaY < 0) {
            delta = - delta;
        }

        zoom(bounds, delta, delta, event.clientX, event.clientY);
        boundsToTransform(bounds);
    }, { passive : true });

    visorImage.addEventListener('mousedown', event => addClass(visorImage, 'dragging'));
    visorImage.addEventListener('mouseup', event => removeClass(visorImage, 'dragging'));

    hammer.on('panstart', event => {
        pan.x = event.deltaX;
        pan.y = event.deltaY;
    });

    hammer.on('panmove', event => {
        move(bounds, event.deltaX - pan.x, event.deltaY - pan.y);
        pan.x = event.deltaX;
        pan.y = event.deltaY;
        boundsToTransform(bounds);
    });
}

function addClass(target, className) {
    target.className += ' ' + className;
}

function removeClass(target, className) {
    let classes = target.className ? target.className.split(' ') : [];
    classes.splice(classes.indexOf(className), 1);
    target.className = classes.join(' ');
}